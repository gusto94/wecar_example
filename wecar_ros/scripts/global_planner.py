#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import rospkg
from math import cos,sin,pi,sqrt,pow,atan2
from geometry_msgs.msg import Point32,PoseStamped,Point,PoseWithCovarianceStamped
from nav_msgs.msg import Odometry,Path,OccupancyGrid
import numpy as np
import tf
from tf.transformations import euler_from_quaternion,quaternion_from_euler

from collections import deque

class globalPlanner :
    def __init__(self):
        rospy.init_node('globalPlanner', anonymous=True)
      
        
        #publisher
        self.path_pub = rospy.Publisher('path',Path, queue_size=1)
        self.is_map=False

        #subscriber
        rospy.Subscriber("/filtered_map", OccupancyGrid, self.map_callback)
        rospy.Subscriber("/move_base_simple/goal", PoseStamped, self.goal_callback)

        self.listener = tf.TransformListener()
        

        self.dx=[-1,0,0,1,-1,-1,1,1]
        self.dy=[0,1,-1,0,-1,1,-1,1]
        self.dCost=[1,1,1,1,1.414,1.414,1.414,1.414]




        rospy.spin()
        


    def map_callback(self,msg):
        
        if self.is_map==False :
            self.map_resolution=msg.info.resolution
            self.map_width=msg.info.width
            self.map_height=msg.info.height
            self.map_offset_x=msg.info.origin.position.x
            self.map_offset_y=msg.info.origin.position.y

            map_to_grid=np.array(msg.data)
            self.grid=np.reshape(map_to_grid,(self.map_width,self.map_height),order='F')
            print('aaaaaaa')
            print(self.grid.shape)
            print(self.map_resolution,self.map_height,self.map_width)
            print('grid_map reshaped')
            self.is_map=True
          

    def goal_callback(self,msg):
        print(msg.pose.position.x,msg.pose.position.y)
        if self.is_map== True:
            try:
                (trans,rot) = self.listener.lookupTransform('/map', '/base_link', rospy.Time(0))
                _,_,yaw=euler_from_quaternion(rot)
                position=[trans[0],trans[1]]

                start_node=self.pose_to_pixel(position)
                goal_node=self.pose_to_pixel([msg.pose.position.x,msg.pose.position.y])
                print(start_node)
                print(goal_node)
                self.dijkstra(start_node,goal_node)
                print('finish')



                # start_pose=self.pixel_to_pose(start_node)
                # print(start_pose)
                

            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                print('[Warning] Wait tf')


    def pose_to_pixel(self,pose):
        map_pixel_x= int((pose[0]-self.map_offset_x)/self.map_resolution)
        map_pixel_y= int((pose[1]-self.map_offset_y)/self.map_resolution)
        return [map_pixel_x,map_pixel_y]
    
    def pixel_to_pose(self,pixel):
        x=(pixel[0]*self.map_resolution)+self.map_offset_x
        y=(pixel[1]*self.map_resolution)+self.map_offset_y
        return [x,y]

    def dijkstra(self,start,goal):
        self.path_msg=Path()
        self.path_msg.header.frame_id='map'

        # if self.grid[start[0]][start[1]] == 0 and self.grid[goal[0]][goal[1]]==0 and start != goal :
        self.path= [[0 for col in range(self.map_height)] for row in range(self.map_width)]
        
        self.cost= np.array([[self.map_height*self.map_width for col in range(self.map_height)] for row in range(self.map_width)])
        print(self.cost.shape,start)
        self.final_path=[]
        Q=deque()
        Q.append(start)
        self.cost[start[0]][start[1]]=1
        count=0
        found= False
        while not len(Q) == 0:
            if found :
                break

            current=Q.popleft()
            for i in range(8):
                next = [current[0]+self.dx[i],current[1]+self.dy[i]]

                if next[0] >= 0 and next[1]>= 0 and next[0] < self.map_width and next[1] <self.map_height :
                    if self.grid[next[0]][next[1]] < 100 :
                        if self.cost[next[0]][next[1]] > self.cost[current[0]][current[1]] + self.dCost[i]:
                            Q.append(next)
                            self.path[next[0]][next[1]]= current
                            self.cost[next[0]][next[1]]= self.cost[current[0]][current[1]]+self.dCost[i]
                            if next==goal: 
                                found=True

        
        node=goal
        while node != start :
            nextNode = self.path[node[0]][node[1]]
            self.final_path.append(node)
            node=nextNode

        if len(self.final_path)  > 0:
            for grid_cell in reversed(self.final_path):
                tmp_pose=PoseStamped()
                waypoint=self.pixel_to_pose(grid_cell)
                tmp_pose.pose.position.x=waypoint[0]
                tmp_pose.pose.position.y=waypoint[1]
                tmp_pose.pose.orientation.w=1.0
                self.path_msg.poses.append(tmp_pose)
                self.path_pub.publish(self.path_msg) 
        else : 
            print('[Warning] Cannot find path')



if __name__ == '__main__':
    try:
        test_track=globalPlanner()
    except rospy.ROSInterruptException:
        pass

