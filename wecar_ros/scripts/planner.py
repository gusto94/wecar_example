#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import rospkg
from sensor_msgs.msg import LaserScan,PointCloud,Imu
from std_msgs.msg import Float64
from vesc_msgs.msg import VescStateStamped
from laser_geometry import LaserProjection
from math import cos,sin,pi,sqrt,pow,atan2
from geometry_msgs.msg import Point32,PoseStamped,Point,PoseWithCovarianceStamped
from nav_msgs.msg import Odometry,Path
from utils import pathReader, findLocalPath

import tf
from tf.transformations import euler_from_quaternion,quaternion_from_euler


class ros_planner :
    def __init__(self):
        rospy.init_node('ros_planner', anonymous=True)
        
        
        #publisher
        self.motor_pub = rospy.Publisher('commands/motor/speed',Float64, queue_size=1)
        self.servo_pub = rospy.Publisher('commands/servo/position',Float64, queue_size=1)

        #subscriber
        rospy.Subscriber("/odom", Odometry, self.odom_callback)

        #class        
        path_reader=pathReader('wecar_ros')
        
        #read path
        global_path=path_reader.read_txt("path.txt")

        #time var
        count = 0
        rate = rospy.Rate(30) # 30hz

        self.is_odom=False

        self.current_postion = Point()

        self.motor_msg=Float64()
        self.servo_msg=Float64()
        
        self.forward_point=Point()
        
        self.is_look_forward_point=False
        self.vehicle_length=1
        self.lfd=0.5
        self.steering=0

        self.steering_angle_to_servo_gain =-1.2135
        self.steering_angle_to_servo_offset=0.5304   
        
        while not rospy.is_shutdown():            

            if self.is_odom == True :
                
                local_path,current_waypoint=findLocalPath(global_path,self.current_postion)   
                    
                vehicle_position=self.current_postion
                rotated_point=Point()
                self.is_look_forward_point= False
        
                for num,i in enumerate(local_path.poses) :
                    path_point=i.pose.position
                    dx= path_point.x - vehicle_position.x
                    dy= path_point.y - vehicle_position.y
                    rotated_point.x=cos(self.vehicle_yaw)*dx +sin(self.vehicle_yaw)*dy
                    rotated_point.y=sin(self.vehicle_yaw)*dx - cos(self.vehicle_yaw)*dy      
                            
                    if rotated_point.x>0 :
                        dis=sqrt(pow(rotated_point.x,2)+pow(rotated_point.y,2))
                        if dis>= self.lfd :
                            self.forward_point=path_point
                            self.is_look_forward_point=True                                
                            break
                                    
                theta=-atan2(rotated_point.y,rotated_point.x)
                if self.is_look_forward_point :
                    self.steering=atan2((2*self.vehicle_length*sin(theta)),self.lfd) #rad
                    # print(self.steering*180/pi) #degree
                    self.motor_msg.data=4000
                else : 
                    self.steering=0
                    print("no found forward point")
                    self.motor_msg.data=0

                    
                self.steering_command=(self.steering_angle_to_servo_gain*self.steering)+self.steering_angle_to_servo_offset 
                print(self.steering_command)
                self.servo_msg.data=self.steering_command
                    
                self.servo_pub.publish(self.servo_msg)
                self.motor_pub.publish(self.motor_msg)

        rate.sleep()

    def odom_callback(self,msg):
        self.is_odom=True        
        odom_quaternion=(msg.pose.pose.orientation.x,msg.pose.pose.orientation.y,msg.pose.pose.orientation.z,msg.pose.pose.orientation.w)
        _,_,self.vehicle_yaw=euler_from_quaternion(odom_quaternion)
        self.current_postion.x=msg.pose.pose.position.x
        self.current_postion.y=msg.pose.pose.position.y    

if __name__ == '__main__':
    try:
        test_track=ros_planner()
    except rospy.ROSInterruptException:
        pass