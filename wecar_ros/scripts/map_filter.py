#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import rospkg
from math import cos,sin,pi,sqrt,pow,atan2
from geometry_msgs.msg import Point32,PoseStamped,Point,PoseWithCovarianceStamped,Pose
from nav_msgs.msg import Odometry,Path,OccupancyGrid,MapMetaData
import numpy as np
import tf
from tf.transformations import euler_from_quaternion,quaternion_from_euler


class mapFilter :
    def __init__(self):
        rospy.init_node('mapFilter', anonymous=True)
      
        
        #publisher
        self.map_pub = rospy.Publisher('filtered_map',OccupancyGrid, queue_size=1)
        self.is_map=False
        self.map_msg=OccupancyGrid()
        #subscriber
        rospy.Subscriber("/map", OccupancyGrid, self.map_callback)


        rate = rospy.Rate(0.2) # 0.2hz
        while not rospy.is_shutdown():    
            

            if self.is_map==True:
                self.map_pub.publish(self.map_msg)
            rate.sleep()




    def map_callback(self,msg):
         
        if self.is_map==False:
            self.map_resolution=msg.info.resolution
            self.map_width=msg.info.width
            self.map_height=msg.info.height
            self.map_offset_x=msg.info.origin.position.x
            self.map_offset_y=msg.info.origin.position.y


            self.map_msg.header=msg.header
            self.map_msg.info=msg.info

            # print(type())
            map_data=np.array(msg.data)
            map_grid=np.reshape(map_data,(self.map_height,self.map_width))

            for x in range(self.map_height):
                for y in range(self.map_width):
                    if map_grid[x][y]==100 :
                        for box_x in range(-9,10):
                            for box_y in range(-9,10):
                                if  0 < x+box_x < self.map_height and 0 < y+box_y< self.map_width and map_grid[x+box_x][y+box_y]!=100 : 
                                    map_grid[x+box_x][y+box_y]=127
                                    
                       
                    
            
            filtered_map_data=map_grid.reshape(1,self.map_width*self.map_height)
            list_filtered_map_data=filtered_map_data.tolist()

            self.map_msg.data=list_filtered_map_data[0]

            print(map_grid.shape,self.map_width,self.map_height)



  







            self.is_map=True 



if __name__ == '__main__':
    try:
        test_track=mapFilter()
    except rospy.ROSInterruptException:
        pass