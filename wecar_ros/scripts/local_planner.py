#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import rospkg
from math import cos,sin,pi,sqrt,pow,atan2
from geometry_msgs.msg import Point32,PoseStamped,Point,PoseWithCovarianceStamped
from nav_msgs.msg import Odometry,Path,OccupancyGrid
import numpy as np
import tf
from tf.transformations import euler_from_quaternion,quaternion_from_euler

class localPlanner :
    def __init__(self):
        rospy.init_node('localPlanner', anonymous=True)
      
        
        #publisher
    
        self.path_pub = rospy.Publisher('local_path',Path, queue_size=1)
        self.is_path=False
        self.local_path_size=100
        #subscriber
        rospy.Subscriber("path", Path, self.path_callback)
        self.listener = tf.TransformListener()
        

        rate = rospy.Rate(30) # 20hz
        while not rospy.is_shutdown():
            if self.is_path==True :
                try:
                    (trans,rot) = self.listener.lookupTransform('/map', '/base_link', rospy.Time(0))
                    position=[trans[0],trans[1]]

                    
                    local_path_msg=Path()
                    local_path_msg.header.frame_id='/map'
                
                    x=trans[0]
                    y=trans[1]
                    min_dis=float('inf')
                    current_waypoint=-1
                    for i,waypoint in enumerate(self.global_path_msg.poses) :

                        distance=sqrt(pow(x-waypoint.pose.position.x,2)+pow(y-waypoint.pose.position.y,2))
                        if distance < min_dis :
                            min_dis=distance
                            current_waypoint=i

                
                    if current_waypoint != -1 :
                        if current_waypoint + self.local_path_size < len(self.global_path_msg.poses):
                            for num in range(current_waypoint,current_waypoint + self.local_path_size ) :
                                tmp_pose=PoseStamped()
                                tmp_pose.pose.position.x=self.global_path_msg.poses[num].pose.position.x
                                tmp_pose.pose.position.y=self.global_path_msg.poses[num].pose.position.y
                                tmp_pose.pose.orientation.w=1
                                local_path_msg.poses.append(tmp_pose)
                        
                        else :
                            for num in range(current_waypoint,len(self.global_path_msg.poses) ) :
                                tmp_pose=PoseStamped()
                                tmp_pose.pose.position.x=self.global_path_msg.poses[num].pose.position.x
                                tmp_pose.pose.position.y=self.global_path_msg.poses[num].pose.position.y
                                tmp_pose.pose.orientation.w=1
                                local_path_msg.poses.append(tmp_pose)



                    self.path_pub.publish(local_path_msg)



               

                except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                    print('[Warning] Wait tf')

            rate.sleep()


        


    def path_callback(self,msg):
        self.is_path=True
        self.global_path_msg=msg
          



if __name__ == '__main__':
    try:
        test_track=localPlanner()
    except rospy.ROSInterruptException:
        pass
